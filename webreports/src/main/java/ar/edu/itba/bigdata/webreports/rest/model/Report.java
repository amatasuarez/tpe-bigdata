package ar.edu.itba.bigdata.webreports.rest.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Report extends ArrayList<Row> {
	private static final long serialVersionUID = -3282181205894252827L;

}
