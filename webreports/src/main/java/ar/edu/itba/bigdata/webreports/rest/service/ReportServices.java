package ar.edu.itba.bigdata.webreports.rest.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ar.edu.itba.bigdata.webreports.rest.dao.ReportDAO;
import ar.edu.itba.bigdata.webreports.rest.dao.RowBuilder;
import ar.edu.itba.bigdata.webreports.rest.model.Report;
import ar.edu.itba.bigdata.webreports.rest.model.Row;
import ar.edu.itba.bigdata.webreports.rest.utils.Constants;

@Path("/Report")
public class ReportServices {
	
	/* BATCH REPORTS
	 * ***********************************************************************************************
	 * */
	
	@GET
	@Path("/AdsByChannel")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findAdsByChannel() {
		return ReportDAO.findAll("AdsByChannel", Constants.VIEWERS, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();			
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/AverageViewersByChannel")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findAverageViewersByChannel() {
		return ReportDAO.findTopChannel("AverageViewersByChannel", new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.DATE_ISO, rs.getString(Constants.DATE_ISO));
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.AVERAGE, rs.getFloat(Constants.AVERAGE));
				return row;
			}
		});
	}
	
	@GET
	@Path("/AverageViewersByCategory")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findAverageViewersByCategory() {
		return ReportDAO.findAll("AverageViewersByCategory", Constants.DATE_ISO,  new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();			
				row.put(Constants.DATE_ISO, rs.getString(Constants.DATE_ISO));
				row.put(Constants.CATEGORY, rs.getString(Constants.CATEGORY));
				row.put(Constants.AVERAGE, rs.getFloat(Constants.AVERAGE));
				return row;
			}
		});
	}
	
	
	@GET
	@Path("/Top10Channel")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findTop10Channel() {
		return ReportDAO.findAll("Top10Channel", Constants.VIEWERS, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();			
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	
	@GET
	@Path("/Top10Category")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findTop10Category() {
		return ReportDAO.findAll("Top10Category", Constants.VIEWERS, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();			
				row.put(Constants.CATEGORY, rs.getString(Constants.CATEGORY));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/Top5ChannelByFamilyGroup")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findTop5ChannelByFamilyGroup() {
		return ReportDAO.findAll("Top5ChannelByFamilyGroup", Constants.VIEWERS, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();				
				row.put(Constants.CUSTOMER_INFO, rs.getString(Constants.CUSTOMER_INFO));
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/Top5ChannelByClientType")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findTop5ChannelByClientType() {
		return ReportDAO.findAll("Top5ChannelByClientType", Constants.VIEWERS, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();				
				row.put(Constants.CUSTOMER_INFO, rs.getString(Constants.CUSTOMER_INFO));
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	
	/* REAL TIME REPORTS
	 * ***********************************************************************************************
	 * */

	@GET
	@Path("/ViewersByChannel")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findViewersByChannel(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findTopChannel("ViewersByChannel", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/ViewersByCategory")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findViewersByCategory(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findAll("ViewersByCategory", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);				
				row.put(Constants.CATEGORY, rs.getString(Constants.CATEGORY));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/ViewersByFamilyGroup")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findViewersByFamilyGroup(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findAll("ViewersByFamilyGroup", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);
				row.put(Constants.CUSTOMER_INFO, rs.getString(Constants.CUSTOMER_INFO));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/ViewersByClientType")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findViewersByClientType(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findAll("ViewersByClientType", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);
				row.put(Constants.CUSTOMER_INFO, rs.getString(Constants.CUSTOMER_INFO));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/TotalViewers")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findTotalViewers(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findAll("TotalViewers", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	@GET
	@Path("/ViewersWatchingAds")
	@Produces({ MediaType.APPLICATION_JSON })
	public Report findViewersWatchingAds(@QueryParam(Constants.ID) int id) {
		return ReportDAO.findTopChannel("ViewersWatchingAds", id, new RowBuilder(){
			@Override
			public Row build(ResultSet rs) throws SQLException {
				Row row = new Row();
				row.put(Constants.ID, rs.getInt(Constants.ID));
				row.put(Constants.TIMESTAMP, rs.getLong(Constants.MINUTE) * 60000);
				row.put(Constants.CHANNEL_NAME, rs.getString(Constants.CHANNEL_NAME));
				row.put(Constants.VIEWERS, rs.getInt(Constants.VIEWERS));
				return row;
			}
		});
	}
	
	
}
