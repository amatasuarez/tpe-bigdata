package ar.edu.itba.bigdata.webreports.rest.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.edu.itba.bigdata.webreports.rest.model.Report;

public abstract class ReportDAO {
		
	private static Connection connection;
	private static Properties properties = new Properties();
	
	private static String JDBC_CONNECTION_URL;
	private static String JDBC_CONNECTION_USERNAME;
	private static String JDBC_CONNECTION_PASSWORD;
		
	static {
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
			
			JDBC_CONNECTION_URL = properties.getProperty("jdbc_connection_url");
			JDBC_CONNECTION_USERNAME = properties.getProperty("jdbc_connection_username");
			JDBC_CONNECTION_PASSWORD = properties.getProperty("jdbc_connection_password");
			Class.forName("com.mysql.jdbc.Driver"); 
			connection = DriverManager.getConnection(JDBC_CONNECTION_URL, JDBC_CONNECTION_USERNAME, JDBC_CONNECTION_PASSWORD);
		} catch (Exception e) {
			Logger.getLogger(ReportDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public static Report findAll(String reportName, int id, RowBuilder rb) {
		Report report = new Report();
		try {
			PreparedStatement ps;
			
			ps = connection.prepareStatement("SELECT * FROM " + reportName +
				" WHERE id > ? AND minute >= (SELECT MAX(minute) FROM " + reportName + ") - 60" +
				" ORDER BY id ASC");
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				report.add(rb.build(rs));
		
		} catch (SQLException e) {
			Logger.getLogger(ReportDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		}
		
		return report;
	}
	
	public static Report findAll(String reportName, String orderBy, RowBuilder rb) {
		Report report = new Report();
		try {
			PreparedStatement ps;
			String query = "SELECT * FROM " + reportName;
			if(orderBy != null)
				query += " ORDER BY " + orderBy;
			ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				report.add(rb.build(rs));
			
		} catch (SQLException e) {
			Logger.getLogger(ReportDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		}

		return report;
	}
	
	public static Report findTopChannel(String reportName, int id, RowBuilder rb) {
		Report report = new Report();
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement(
				"SELECT * FROM " + reportName + " WHERE id > ? AND minute >= (SELECT MAX(minute) FROM " + reportName + ") - 60 AND channelName IN " +
				"(SELECT * FROM (SELECT DISTINCT channelName " +
				"FROM " + reportName + " WHERE minute >= (SELECT MAX(minute) FROM " + reportName + ") - 60 " +
				"ORDER BY viewers DESC LIMIT 10) AS t);"
			);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				report.add(rb.build(rs));
			
		} catch (SQLException e) {
			Logger.getLogger(ReportDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		}

		return report;
	}
	
	public static Report findTopChannel(String reportName, RowBuilder rb) {
		Report report = new Report();
		try {
			PreparedStatement ps;
			ps = connection.prepareStatement(
				"SELECT * FROM " + reportName + " WHERE channelName IN " +
				"(SELECT * FROM (SELECT DISTINCT channelName " +
				"FROM " + reportName + " WHERE dateISO >= (SELECT MAX(dateISO) FROM " + reportName + ") - INTERVAL 1 WEEK " +
				"ORDER BY average DESC LIMIT 10) AS t) ORDER BY dateISO;"
			);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				report.add(rb.build(rs));
			
		} catch (SQLException e) {
			Logger.getLogger(ReportDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		}

		return report;
	}
	
}
