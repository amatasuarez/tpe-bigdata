package ar.edu.itba.bigdata.webreports.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import ar.edu.itba.bigdata.webreports.rest.model.Row;

public interface RowBuilder {
	
	public Row build(ResultSet rs) throws SQLException ;
	
}
