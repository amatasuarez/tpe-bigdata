package ar.edu.itba.bigdata.webreports.rest.utils;

public class Constants {

	public static final String CHANNEL_NAME = "channelName";
	public static final String TIMESTAMP = "timestamp";
	public static final String MINUTE = "minute";
	public static final String CATEGORY = "category";
	public static final String VIEWERS = "viewers";
	public static final String ID = "id";
	public static final String CUSTOMER_INFO = "customerInfo";
	public static final String DATE_ISO = "dateISO";
	public static final String AVERAGE = "average";
	
	
}
