function Log(){
	this.panel = $('#logpanel');
}

Log.prototype.info = function(msg){
	var ts = new Date();
	var t = (ts.getHours() + 1) + ':' + ts.getMinutes() + ':' + ts.getSeconds();
	
	this.panel.val(this.panel.val() + ($.datepicker.formatDate('dd-mm-yy', ts) + ' ' + t) + ' - INFO: ' + msg + '\n');
	
	this.panel.scrollTop(this.panel[0].scrollHeight);
}

Log.prototype.clear = function(){
	this.panel.val('');
}