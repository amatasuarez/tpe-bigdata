var viewersByChannel = new Report({	name: 'ViewersByChannel', label: 'Televidentes por canal', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'}, {type: 'string', id: 'channelName', label: 'Canal'} ] });

var viewersByCategory = new Report({ name: 'ViewersByCategory', label: 'Televidentes por categoría', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'}, {type: 'string', id: 'category', label: 'Categoría'} ] });

var viewersByFamilyGroup = new Report({	name: 'ViewersByFamilyGroup', label: 'Televidentes por grupo familiar', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'}, {type: 'string', id: 'customerInfo', label: 'Grupo familiar'} ] });

var viewersByClientType = new Report({ name: 'ViewersByClientType', label: 'Televidentes por tipo de cliente', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'}, {type: 'string', id: 'customerInfo', label: 'Tipo de cliente'} ] });

var totalViewers = new Report({	name: 'TotalViewers', label: 'Televidentes totales', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'} ] });

var viewersWatchingAds = new Report({ name: 'ViewersWatchingAds', label: 'Televidentes mirando anuncios', isRealTime: true, type: 'spline',
	columns: [ {type: 'number', id: 'id', label: 'ID'}, {type: 'datetime', id: 'timestamp', label: 'Minuto'},
	           {type: 'number', id: 'viewers', label: 'Televidentes'}, {type: 'string', id: 'channelName', label: 'Canal'} ] });

// Real time report events
function bindRealTimeReports() {
	// ViewersByChannel
	$('#viewersByChannel').click(function(){
		var report = viewersByChannel;				
		report.chart('timestamp', 'viewers', 'channelName');
	});
	
	// ViewersByCategory
	$('#viewersByCategory').click(function(){
		var report = viewersByCategory;			
		report.chart('timestamp', 'viewers', 'category');		
	});
	
	// ViewersByFamilyGroup
	$('#viewersByFamilyGroup').click(function(){
		var report = viewersByFamilyGroup;		
		report.chart('timestamp', 'viewers', 'customerInfo');
	});
	
	// ViewersByClientType
	$('#viewersByClientType').click(function(){
		var report = viewersByClientType;
		report.chart('timestamp', 'viewers', 'customerInfo');
	});
	
	// TotalViewers
	$('#totalViewers').click(function(){
		var report = totalViewers;				
		report.chart('timestamp', 'viewers');
	});
	
	// ViewersWatchingAds
	$('#viewersWatchingAds').click(function(){
		var report = viewersWatchingAds;
		report.chart('timestamp', 'viewers', 'channelName');
	});
};