var adsByChannel = new Report({	name: 'AdsByChannel', label: 'Anuncios por canal', isRealTime: false, type: 'column', 
	columns: [ {type: 'string', id: 'channelName', label: 'Canal'}, {type: 'number', id: 'viewers', label: 'Televidentes'}] });

var averageViewersByChannel = new Report({	name: 'AverageViewersByChannel', label: 'Promedio televidentes por canal', isRealTime: false, type: 'spline',
	columns: [ {type: 'string', id: 'dateISO', label: 'Fecha'}, 
	           {type: 'string', id: 'channelName', label: 'Canal'}, {type: 'number', id: 'average', label: 'Televidentes'}] });

var averageViewersByCategory = new Report({	name: 'AverageViewersByCategory', label: 'Promedio televidentes por categoría', isRealTime: false, type: 'spline',
	columns: [ {type: 'string', id: 'dateISO', label: 'Fecha'}, 
	           {type: 'string', id: 'category', label: 'Categoría'}, {type: 'number', id: 'average', label: 'Televidentes'}] });

var top10Channel = new Report({	name: 'Top10Channel', label: 'Top 10 canales', isRealTime: false, type: 'column',
	columns: [ {type: 'string', id: 'channelName', label: 'Canal'}, {type: 'number', id: 'viewers', label: 'Televidentes'}] });

var top10Category = new Report({ name: 'Top10Category', label: 'Top 10 categorías', isRealTime: false, type: 'bar',
	columns: [ {type: 'string', id: 'category', label: 'Categoría'}, {type: 'number', id: 'viewers', label: 'Televidentes'}] });

var top5ChannelByFamilyGroup = new Report({	name: 'Top5ChannelByFamilyGroup', label: 'Top 5 canales por grupo familiar', isRealTime: false, type: 'column',
	columns: [ {type: 'string', id: 'customerInfo', label: 'Grupo familiar'},
	           {type: 'string', id: 'channelName', label: 'Canal'}, {type: 'number', id: 'viewers', label: 'Televidentes'}] });

var top5ChannelByClientType = new Report({ name: 'Top5ChannelByClientType', label: 'Top 5 canales por tipo de cliente', isRealTime: false, type: 'column',
	columns: [{type: 'string', id: 'customerInfo', label: 'Tipo de cliente'},
	          {type: 'string', id: 'channelName', label: 'Canal'}, {type: 'number', id: 'viewers', label: 'Televidentes'}] });

// Batch report events
function bindBatchReports() {
	// AdsByChannel
	$('#adsByChannel').click(function(){
		var report = adsByChannel;
		var options = {oneGroup: true};
		report.chart('channelName', 'viewers', 'channelName', options);
	});
		
	// AverageViewersByChannel
	$('#averageViewersByChannel').click(function(){
		var report = averageViewersByChannel; 
		report.chart('dateISO', 'average', 'channelName');
	});
	
	// AverageViewersByCategory
	$('#averageViewersByCategory').click(function(){
		var report = averageViewersByCategory; 
		report.chart('dateISO', 'average', 'category');
	});
	
	// Top10Channel
	$('#top10Channel').click(function(){
		var report = top10Channel; 
		var options = {oneGroup: true, yMin: 450};
		report.chart('channelName', 'viewers', 'channelName', options);
	});
	
	// Top10Category
	$('#top10Category').click(function(){
		var report = top10Category; 
		var options = {oneGroup: true};
		report.chart('category', 'viewers', 'category', options);
	});
	
	// Top5ChannelByFamilyGroup
	$('#top5ChannelByFamilyGroup').click(function(){
		var report = top5ChannelByFamilyGroup; 
		var options = {oneGroup: false, stacking: 'normal'};
		report.chart('customerInfo', 'viewers', 'channelName', options);
	});
	
	// Top5ChannelByClientType
	$('#top5ChannelByClientType').click(function(){
		var report = top5ChannelByClientType;		
		var options = {oneGroup: false, stacking: 'normal'};
		report.chart('customerInfo', 'viewers', 'channelName', options);
	});
};
