Report.DATA_POLLING_INTERVAL = 5000;
Report.CHART_DATA_POLLING_INTERVAL = 100; 

Report.current = null;

function Report(context){
	this.name = context.name;
	this.label = context.label;
	this.type = context.type;
	this.isRealTime = context.isRealTime;
	this.columns = [];
	for (var i=0; i < context.columns.length; i++)
		this.columns[context.columns[i].id] = context.columns[i];

	// Intervals
	this.dataPollerInterval = null;
	this.chartDataPollerInterval = null;
	
	// 'raw' is a JSON copy of this report's data
	this.raw = [];
	this.lastIdFound = 0;
}

Report.prototype.restart = function () {
	if (Report.current != null)
		Report.current.stop();
	
	this.raw = [];
	this.lastIdFound = 0;

	// Clear intervals
	clearInterval(this.dataPollerInterval);
	clearInterval(this.chartDataPollerInterval);
	
	Report.current = this;
	
	log.info('Started/restarted report: "' + this.name + '"');

	this.setPoller();
};

Report.prototype.pause = function () {
	if (this.dataPollerInterval != null)
		clearInterval(this.dataPollerInterval);
	
	if (this.chartDataPollerInterval != null)
		clearInterval(this.chartDataPollerInterval);

	log.info('Paused report: "' + this.name + '"');
};

Report.prototype.stop = function () {
	this.pause();
	Report.current = null;
	
	log.info('Stopped report: "' + this.name + '"');
};

Report.prototype.resume = function () {	
	log.info('Resumed report: "' + this.name + '"');
	this.setPoller();	
};

Report.prototype.setPoller = function () {
	// Set the interval and call the function now.
	if(this.isRealTime)
		this.dataPollerInterval = setInterval(function () {		
			api.getReport(Report.current);
		}, Report.DATA_POLLING_INTERVAL);
	
	api.getReport(Report.current);
};

var currentChart;

// Highcharts graph
Report.prototype.chart = function(xName, yName, serieName, options){
	var thisReport = this;
	var onChartLoad = null;
	if(!options)
		options = {};
	
	if(serieName != null)
		onChartLoad = function(event) {
			var chart = this;		
			currentChart = chart;
	        thisReport.chartDataPollerInterval = setInterval(function() {	     
	        	var newData = false;
	        	while(r = thisReport.raw.shift()) {
	        		newData = true;
	        		var serieLabel = r[serieName];
	        		if (!chart.series[serieLabel])
	        			chart.series[serieLabel] = chart.addSeries({ name: serieLabel, data: [] });
	        			        		
	        		var xValue = r[xName];	        		
	        		
	        		// Group in categories
	        		if(!options.oneGroup && !chart.xAxis[0].isDatetimeAxis) {
	        			var categories = chart.xAxis[0].categories;
		        		if(!categories)
		        			categories = [];
		        		var index = $.inArray(r[xName], categories);
		        		if(index == -1) {
		        			categories.push(r[xName]);
		        			index = categories.length - 1;
		        			chart.xAxis[0].setCategories(categories, false);
		        		}
		        		xValue = index;
	        		}
	        			        		
	        		// Slice old data (1 hour before current time) and append new one	        		
	        		var slice = chart.xAxis[0].isDatetimeAxis && 
	        				(chart.xAxis[0].dataMin && chart.xAxis[0].dataMin + 3660000 <= xValue);	        
	        		
	        		chart.series[serieLabel].addPoint([xValue, r[yName]], false, slice);	        		
	        	}
	        	if(newData)
	        		chart.redraw();
	        }, Report.CHART_DATA_POLLING_INTERVAL);
    	};
	else
		onChartLoad = function(event) {
			var chart = this;			
			var series = chart.addSeries({ name: yName, data: [] });
			thisReport.chartDataPollerInterval = setInterval(function() {
				var newData = false;
				while(r = thisReport.raw.shift()) { 
					newData = true;
            		series.addPoint([r[xName], r[yName]], false);
				}
				if(newData)
	        		chart.redraw();
			}, Report.CHART_DATA_POLLING_INTERVAL);
		};
	
    
	thisReport.restart();
	$('#chartContainer div.chart').highcharts({
        chart: {
            type: thisReport.type,
            height: 600,
            events: { load: onChartLoad }
        },
        title: { text: thisReport.label },
        xAxis: { title: { text: thisReport.columns[xName].label }, type: thisReport.columns[xName].type, tickPixelInterval: 100,
        	labels: {
	            formatter: function () {
	            	if(this.chart.xAxis[0].isDatetimeAxis) {
    	          		return Highcharts.dateFormat('%H:%M', this.value);	 
    	          	} else
	            		return this.value;
	            }
        	}
        },        
        tooltip: { xDateFormat: '%H:%M' },           
        yAxis: { title: { text: thisReport.columns[yName].label }, type: thisReport.columns[yName].type, min: options.yMin },
        legend: { enabled: true },
        exporting: { enabled: false },
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    states: { hover: { enabled: true } }
                },
        		stacking: options.stacking
            }
        }
    });
};