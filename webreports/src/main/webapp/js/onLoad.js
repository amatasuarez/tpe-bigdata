var log;
var api = new Api();

$(document).ready(onLoad);

function onLoad(){
	
	log = new Log();
	log.clear();
	
	$('#menu-batch').menu().hide();
	$('#menu-realtime').menu().hide();
	
	$(document).click(function(n){
		if(n.target.id == 'menu-batch-button'){
			$('#menu-batch').toggle();
			$('#menu-realtime').hide();
		} else if(n.target.id == 'menu-realtime-button'){
			$('#menu-realtime').toggle();
			$('#menu-batch').hide();
		} else {
			$('#menu-batch').hide();
			$('#menu-realtime').hide();
		}
			
		return false;
	});
	
	$(document).ajaxStart(function() {
		log.info('Checking for incoming tuples...');
	});		
	
	bindBatchReports();
	bindRealTimeReports();
	
}