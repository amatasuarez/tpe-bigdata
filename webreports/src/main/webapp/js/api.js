var retrievingData = false;

function Api(){
	
	this.getReport = function(report){
		if(retrievingData)
			return;
		
		retrievingData = true;
		$.ajax({
			type: 'GET',
			url: 'json/Report/' + report.name + '?id=' + report.lastIdFound,
			cache: false,
			global: true,
			dataType: 'json',
			success: function(json){
				retrievingData = false;
				
				if(json.length > 0) {
					// Raw JSON.
					report.raw.push.apply(report.raw, json);
					
					// Update Report's lastIdFound
					report.lastIdFound = json[json.length-1].id;
					
					log.info('Fetched ' + json.length + ' tuples.');
				} else
					log.info('No tuples to fetch.');				
			}
		});
	};
		
}