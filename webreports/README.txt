Para cargar el proyecto en Eclipse:

$> mvn eclipse:eclipse

Para compilar:

$> mvn clean package

Para mover el .war generado al servidor Tomcat:

$> scp -i id_dsa webreports.war hadoop@hadoop-2013-datanode-9:/storage/apache-tomcat-6.0.37/webapps/


