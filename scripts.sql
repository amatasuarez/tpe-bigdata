# Para conectarse a MySql (desde hadoop-2013-datanode-6):
# usuario root, pass root, database test_aa
#		mysql test_aa;

####################################################################

# Para crear la base de datos 'test_aa'
CREATE DATABASE test_aa DEFAULT CHARACTER SET utf8;

# Para crear las tablas de los reportes batch

CREATE TABLE Top10Channel(
	channelName	VARCHAR(32),
	viewers	INT
);
CREATE TABLE Top10Category(
	category VARCHAR(32),
	viewers	INT
);
CREATE TABLE AdsByChannel(
	channelName	VARCHAR(32),
	viewers	INT
);
CREATE TABLE AverageViewersByChannel(
	dateISO DATE,
	channelName VARCHAR(32),
	average FLOAT
);
CREATE TABLE AverageViewersByCategory(
	dateISO  DATE,
	category VARCHAR(32),
	average  FLOAT
);
CREATE TABLE Top5ChannelByFamilyGroup(
	customerInfo VARCHAR(32),	
	channelName	VARCHAR(32),
	viewers   INT
);
CREATE TABLE Top5ChannelByClientType(
	customerInfo VARCHAR(32),	
	channelName	VARCHAR(32),
	viewers	INT
);

#############################################################

# Para hacer el dump sobre alguna tabla existente

SELECT * INTO OUTFILE '/tmp/output.csv' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' FROM table_name;

#############################################################