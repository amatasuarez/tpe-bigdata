package ar.edu.itba.bigdata.bolt;

import ar.edu.itba.bigdata.utils.SQLUtils;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class FieldsDBOutputBolt extends AbstractDBOutputBolt {
	private static final long serialVersionUID = 1L;
	
	private String[] fieldNames;
	
	public FieldsDBOutputBolt(String tableName, String... fieldNames) {
		super(tableName);
		this.fieldNames = fieldNames;
	}
	
	@Override
	protected String createTableQuery() {
		return SQLUtils.createTableIfNotExists(tableName, fieldNames);
	}

	@Override
	protected String insertIntoQuery(Tuple t) {
		Fields fields = t.getFields();
		
		String[] values = new String[fields.size()];
		for (int i=0; i < fields.size(); i++)
			values[i] = String.valueOf(t.getValueByField(fields.get(i)));
		
		return SQLUtils.insertInto(tableName, fieldNames, values);
	}

}
