package ar.edu.itba.bigdata.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class StdOutBolt  extends BaseBasicBolt {
	private static final long serialVersionUID = 1L;
	
	@Override
    public void execute(Tuple t, BasicOutputCollector collector) {			        
        Fields fields = t.getFields();
		StringBuffer buf = new StringBuffer();
				
		for (String fieldName : fields)			
			buf.append(String.valueOf(t.getValueByField(fieldName)) + ", ");		
		
		String line = buf.substring(0, buf.length() - 2);
		System.out.println(line);		
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer ofd) {
    	
    }
        
}