package ar.edu.itba.bigdata.bolt;

import java.util.HashMap;
import java.util.Map;

import ar.edu.itba.bigdata.bolt.TurnedOnTVsBolt.CustomerInfo;
import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CountByCustomerInfoBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	
	OutputCollector _collector;
	
	private CustomerInfo info;
	private long currentMinute = -1;
	private Map<Object, Integer> counts = new HashMap<Object, Integer>();
	
	public CountByCustomerInfoBolt(CustomerInfo info){
		super();
		this.info = info;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        _collector = collector;		
	}

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.MINUTE, Constants.CUSTOMER_INFO, Constants.VIEWERS));
    }

	@Override
	public void execute(Tuple t) {
		long minute = t.getLongByField(Constants.MINUTE);
		
		// If new minute, emit previous minute values
		if (currentMinute < minute){
			if (currentMinute != -1)
				for (Object key : counts.keySet()){
					_collector.emit(new Values(currentMinute, key, counts.get(key))); 					
				}
			currentMinute = minute;
		}

		Object infoValue = getInfoValue(t);
		Integer accumulated = counts.get(infoValue);
		if (accumulated == null)
			accumulated = 0;

		int diff = t.getIntegerByField(Constants.DIFF);
		counts.put(infoValue, diff == -1 && accumulated == 0 ? accumulated : accumulated + diff);
		
        _collector.ack(t);
		
	}
	
	private Object getInfoValue(Tuple t){
		switch (info){
	    	case FAMILY_GROUP:
	    		return t.getStringByField(Constants.FAMILY_GROUP);
	    	default: //case CLIENT_TYPE
	    		return t.getStringByField(Constants.CLIENT_TYPE);
    	}
	}

}
