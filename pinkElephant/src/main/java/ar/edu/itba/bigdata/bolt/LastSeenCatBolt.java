package ar.edu.itba.bigdata.bolt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;


import ar.edu.itba.bigdata.utils.Constants;
import ar.edu.itba.bigdata.utils.HBaseUtils;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class LastSeenCatBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;
	
	// box_id, List<last_seen_cats>
	private Map<Integer, List<String>> lastSeen = new HashMap<Integer, List<String>>();
	private HTable channelTable;
	
	@SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        channelTable = HBaseUtils.getInstance().getHTable("channel");
    }
	
	@Override
	public void cleanup() {
		HBaseUtils.getInstance().closeHTable(channelTable);
	}

    @Override
    public void execute(Tuple t) {    
    	long ts = t.getLongByField(Constants.TIMESTAMP);
    	int box_id = t.getIntegerByField(Constants.BOX_ID);
    	    	    	
    	List<String> lastSeenCats = lastSeen.get(box_id);    	
    	if(lastSeenCats != null)
    		for(String cat: lastSeenCats)
    			_collector.emit(new Values(ts, cat, -1));
    	
    	List<String> newCategories = new ArrayList<String>();
    	Integer newChannel = t.getIntegerByField(Constants.CHANNEL);
    	if(newChannel != null) {
    		String newCategoriesStr = Bytes.toString(HBaseUtils.getInstance().getValue(channelTable, String.valueOf(newChannel), "info", "categories"));    		
    		for(String newCat: newCategoriesStr.split(",")) {
    			newCategories.add(newCat);
    			_collector.emit(new Values(ts, newCat, 1));
    		}    		
    	}
    	
    	lastSeen.put(box_id, newCategories);
    	
        _collector.ack(t);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.TIMESTAMP, Constants.CATEGORY, Constants.DIFF));
    }
}