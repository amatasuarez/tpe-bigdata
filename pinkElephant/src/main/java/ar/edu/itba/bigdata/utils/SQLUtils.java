package ar.edu.itba.bigdata.utils;


public class SQLUtils {

	public static String dropTableIfExists(String table){
		return "DROP TABLE IF EXISTS " + table + ";";
	}
	
	public static String createTableIfNotExists(String table, int fieldsNumber){
	    String[] fields = new String[fieldsNumber];
	    for (int i=0; i < fieldsNumber; i++)
	    	fields[i] = "field_" + i;
	    return createTableIfNotExists(table, fields);
	}
	
	public static String createTableIfNotExists(String table, String... fields){
	    StringBuffer buf = new StringBuffer("CREATE TABLE IF NOT EXISTS " + table + "(id INT PRIMARY KEY AUTO_INCREMENT");
	    for (String f : fields)
	    	buf.append(", " + f + " VARCHAR(25)");
	    return buf.toString() + ");";
	}
	
	public static String insertInto(String table, int fieldsNumber, String[] values){
		String[] fields = new String[fieldsNumber];
	    for (int i=0; i < fieldsNumber; i++)
	    	fields[i] = "field_" + i;
	    return insertInto(table, fields, values);
	}
	
	public static String insertInto(String table, String[] fields, String[] values){
	    StringBuffer buf = new StringBuffer("INSERT INTO " + table + " (");
	    int i = 0;
		for (String f : fields)
        	buf.append(f + (i++ == fields.length - 1 ? ")" : ","));
		
		buf.append(" VALUES (");
		
		i = 0;
		for (String v : values)
			buf.append("'" + v + "'" + (i++ == fields.length - 1 ? ");" : ","));
		
		return buf.toString();
	    
	}
}
