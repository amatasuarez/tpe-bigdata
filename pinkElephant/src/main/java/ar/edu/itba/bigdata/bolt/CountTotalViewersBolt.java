package ar.edu.itba.bigdata.bolt;

import java.util.Map;

import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CountTotalViewersBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	
	OutputCollector _collector;
	
	private int currentViewersCount;
	private long currentMinute = -1;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        _collector = collector;		
	}

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.MINUTE, Constants.VIEWERS));
    }
	
	@Override
	public void execute(Tuple t) {
		long minute = t.getLongByField(Constants.MINUTE);
		
		// If new minute, emit previous minute values
		if (currentMinute < minute){
			if (currentMinute != -1)
				_collector.emit(new Values(currentMinute, currentViewersCount)); 					
			currentMinute = minute;
		}

		int diff = t.getIntegerByField(Constants.DIFF);
		currentViewersCount += diff == -1 && currentViewersCount == 0 ? 0 : diff;
		
        _collector.ack(t);
		
	}
}
