package ar.edu.itba.bigdata.bolt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.edu.itba.bigdata.utils.Constants;
import ar.edu.itba.bigdata.utils.SQLUtils;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public abstract class AbstractDBOutputBolt extends BaseBasicBolt {
	private static final long serialVersionUID = 1L;
	
	protected String tableName;
	private Connection connection;
	
	public AbstractDBOutputBolt(String tableName) {
		this.tableName = tableName;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		connection = null;
        PreparedStatement drop = null;
        PreparedStatement create = null;

        try {
        	Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(Constants.JDBC_CONNECTION_URL,
            		Constants.JDBC_CONNECTION_USERNAME,
            		Constants.JDBC_CONNECTION_PASSWORD);

            String dropQuery = SQLUtils.dropTableIfExists(tableName);
            String createQuery = createTableQuery();

            drop = connection.prepareStatement(dropQuery);
            create = connection.prepareStatement(createQuery);

            drop.executeUpdate();
            create.executeUpdate();

        } catch (Exception e1) {
            Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e1.getMessage(), e1);
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        } finally {
        	try {                
	            if (drop != null)
	                drop.close();
	            
	            if (create != null)
	            	create.close();
	            
            } catch (SQLException e) {
                Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        	
        }
	}
		
	@Override
	public void cleanup() {
		if (connection != null){
			try {
				connection.close();
			} catch (SQLException e) {
                Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                try {
					connection.rollback();
				} catch (SQLException e1) {
	                Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e1.getMessage(), e1);
				}
			}
        }
	}
	@Override
	public void execute(Tuple t, BasicOutputCollector collector) {	
		String insertQuery = insertIntoQuery(t);
		
		Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.INFO, insertQuery);
		
		PreparedStatement insert = null;
		try {
			insert = connection.prepareStatement(insertQuery);
			insert.executeUpdate();
		} catch (SQLException e){
            Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				if (insert != null)
					insert.close();
			} catch (SQLException e) {
	            Logger.getLogger(AbstractDBOutputBolt.class.getName()).log(Level.SEVERE, e.getMessage(), e);
			}
		}
		
	}

	protected abstract String createTableQuery();
	
	protected abstract String insertIntoQuery(Tuple t);

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		
	}

}
