package ar.edu.itba.bigdata.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import ar.edu.itba.bigdata.PinkElephantTopology;

public class Constants {

private static Properties properties = new Properties();
	
	public static String HBASE_ROOTDIR;
	public static String HBASE_ZOOKEEPER_QUORUM;
	public static String HBASE_ZOOKEEPER_PORT; 
	
	public static String JMS_QUEUE_URL;
	public static String JMS_QUEUE_NAME;
		
	public static String JDBC_CONNECTION_URL;
	public static String JDBC_CONNECTION_USERNAME;
	public static String JDBC_CONNECTION_PASSWORD;
	
	public static final String MEDITIONS_SPOUT = "meditionsSpout";	
	public static final String JSON_BOLT = "jsonBolt";
	
	public static final String LAST_SEEN_CHANNEL_BOLT = "lastSeenChannelBolt";
	public static final String COUNT_BY_CHANNEL_BOLT = "countByChannelBolt";
	
	public static final String LAST_SEEN_CAT_BOLT = "lastSeenCatBolt";
	public static final String COUNT_BY_CAT_BOLT = "countByCatBolt";
	
	public static final String TURNED_ON_TV_FAMILY_BOLT = "turnedOnTVsBolt_family";
	public static final String COUNT_BY_FAMILY_BOLT = "countByCustomerInfoBolt_family";
	
	public static final String TURNED_ON_TV_CLIENT_TYPE_BOLT = "turnedOnTVsBolt_clientType";
	public static final String COUNT_BY_CLIENT_TYPE_BOLT = "countByCustomerInfoBolt_clientType";	
	
	public static final String COUNT_BOLT = "countTotalViewersBolt";	
	public static final String TURNED_ON_TV_BOLT = "turnedOnTVsBolt";
	
	public static final String COUNT_BY_CHANNEL_RAW_BOLT = "countByChannelBolt_raw";
	public static final String COUNT_BY_ADS_BOLT = "countByAdsBolt";	
		
	public static final String JSON = "json";
	public static final String CHANNEL = "channel";
	public static final String CHANNEL_NAME = "channelName";
	public static final String TIMESTAMP = "timestamp";
	public static final String MINUTE = "minute";
	public static final String CATEGORY = "category";
	public static final String VIEWERS = "viewers";
	public static final String BOX_ID = "box_id";
	public static final String POWER = "power";
	public static final String DIFF = "diff";
	public static final String FAMILY_GROUP = "familyGroup";
	public static final String CLIENT_TYPE = "clientType";
	public static final String CUSTOMER_INFO = "customerInfo";
	
	
	static {
    	try {
			properties.load(ClassLoader.getSystemResourceAsStream("config.properties"));
			
			HBASE_ROOTDIR = properties.getProperty("hbase_rootdir");
			HBASE_ZOOKEEPER_QUORUM = properties.getProperty("hbase_zookeeper_quorum");
			HBASE_ZOOKEEPER_PORT = properties.getProperty("hbase_zookeeper_port");
			
			JMS_QUEUE_URL = properties.getProperty("jms_queue_url");
			JMS_QUEUE_NAME = properties.getProperty("jms_queue_name");
			
			JDBC_CONNECTION_URL = properties.getProperty("jdbc_connection_url");
			JDBC_CONNECTION_USERNAME = properties.getProperty("jdbc_connection_username");
			JDBC_CONNECTION_PASSWORD = properties.getProperty("jdbc_connection_password");
			
		} catch (IOException e) {
			Logger.getLogger(PinkElephantTopology.class).fatal(e.getMessage());
		}
	}
}
