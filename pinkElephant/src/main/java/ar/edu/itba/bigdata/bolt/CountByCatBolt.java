package ar.edu.itba.bigdata.bolt;

import java.util.HashMap;
import java.util.Map;

import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CountByCatBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;
	
	// category, accumulated_views
	private Map<String, Integer> counts = new HashMap<String, Integer>();
	private long currentMinute = -1L;
	
	@SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    @Override
    public void execute(Tuple t) {    	
    	long ts = t.getLongByField(Constants.TIMESTAMP);
    	long minute = ts / 60L;
    	
    	// If new minute, emit previous minute values
		if (currentMinute < minute) {
			if(currentMinute != -1L) {
				for (String cat : counts.keySet())
					_collector.emit(new Values(currentMinute, cat, counts.get(cat)));					
			}
			currentMinute = minute;
		}
		
    	String cat = t.getStringByField(Constants.CATEGORY);    	    	    	
    	Integer	accumulated = counts.get(cat);
		if(accumulated == null)
			accumulated = 0;
		
		accumulated += t.getIntegerByField(Constants.DIFF);
		counts.put(cat, accumulated);				
        _collector.ack(t);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.MINUTE, Constants.CATEGORY, Constants.VIEWERS));
    }
}