package ar.edu.itba.bigdata.utils;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;


public class HBaseUtils {
	
	private static HBaseUtils instance = null;
	private static Configuration conf;
	
	public static HBaseUtils getInstance() {
		if(instance == null)
			instance = new HBaseUtils();
		return instance;
	}
	
	private HBaseUtils() {
		conf = HBaseConfiguration.create();		
		conf.set("hbase.rootdir", Constants.HBASE_ROOTDIR);
		conf.set("hbase.zookeeper.quorum", Constants.HBASE_ZOOKEEPER_QUORUM);
		conf.set("hbase.zookeeper.property.clientPort", Constants.HBASE_ZOOKEEPER_PORT);
	}

	public byte [] getValue(HTable table, String key, String family, String qualififer) {
		Result result;
		try {			
			Get g = new Get(Bytes.toBytes(key));			
			result = table.get(g);			
		} catch (Exception e) {
			Logger.getLogger(HBaseUtils.class.getName()).log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
		
		return result.getValue(Bytes.toBytes(family), Bytes.toBytes(qualififer));				 
	}
	
	public HTable getHTable(String tableName) {
		try {
			return new HTable(conf, tableName);
		} catch (IOException e) {
			Logger.getLogger(HBaseUtils.class.getName()).log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
	}
	
	public void closeHTable(HTable table){
		try {	
			table.close();
		} catch (IOException e) {
			Logger.getLogger(HBaseUtils.class.getName()).log(Level.SEVERE, e.getMessage(), e);		
		}
	}

	@SuppressWarnings("unused")
	public Boolean isChannelAdvertising(HTable table, Long timestamp, Integer channel) {
		String ts = String.valueOf(timestamp);
		String c = String.valueOf(channel);
		
		// Case 1: start_timestamp = end_timestamp
		FilterList f1 = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		f1.addFilter(new SingleColumnValueFilter("info".getBytes(), "start_timestamp".getBytes(), CompareOp.EQUAL, ts.getBytes()));
		f1.addFilter(new SingleColumnValueFilter("info".getBytes(), "end_timestamp".getBytes(), CompareOp.EQUAL, ts.getBytes()));
		
		// Case 2: start_timestamp < end_timestamp
		FilterList f2 = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		f2.addFilter(new SingleColumnValueFilter("info".getBytes(), "start_timestamp".getBytes(), CompareOp.LESS_OR_EQUAL, ts.getBytes()));
		f2.addFilter(new SingleColumnValueFilter("info".getBytes(), "end_timestamp".getBytes(), CompareOp.GREATER, ts.getBytes()));
		
		// Case 1 OR Case 2
		FilterList or = new FilterList(FilterList.Operator.MUST_PASS_ONE);
		or.addFilter(f1);
		or.addFilter(f2);

		FilterList filters = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		filters.addFilter(new SingleColumnValueFilter("info".getBytes(), "channel_number".getBytes(), CompareOp.EQUAL, c.getBytes()));
		filters.addFilter(new SingleColumnValueFilter("info".getBytes(), "type".getBytes(), CompareOp.EQUAL, "ads".getBytes()));
		filters.addFilter(or);
		
		Scan scan = new Scan();
		scan.setFilter(filters);
		
		ResultScanner scanner = null;
		try {
			scanner = table.getScanner(scan);
			
			// Only zero or one rows are expected as result.
			// That's why it breaks the loop immediately upon entering the for-each clause.
			// Didn't use scanner.iterator().next() just to avoid handling NoSuchElementException.
			 
			for (Result result : scanner)
				return true;
			
			return false;
			
		} catch (IOException e){
			Logger.getLogger(HBaseUtils.class.getName()).log(Level.SEVERE, e.getMessage(), e);
		    return null;
		}
	}
}
