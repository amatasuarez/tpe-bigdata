package ar.edu.itba.bigdata.bolt;

import java.util.Map;

import org.apache.hadoop.hbase.client.HTable;

import ar.edu.itba.bigdata.utils.Constants;
import ar.edu.itba.bigdata.utils.HBaseUtils;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CountByAdsBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	
	OutputCollector _collector;
	private HTable day_partsTable;
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        _collector = collector;		
        day_partsTable = HBaseUtils.getInstance().getHTable("day_parts");
	}
	
	@Override
	public void cleanup() {
		HBaseUtils.getInstance().closeHTable(day_partsTable);
	}

	@Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.MINUTE, Constants.CHANNEL_NAME, Constants.VIEWERS));
    }
	
	@Override
	public void execute(Tuple t) {
		long ts = t.getLongByField(Constants.TIMESTAMP);
		long minute = t.getLongByField(Constants.MINUTE);
		Integer channel = t.getIntegerByField(Constants.CHANNEL);
		String channelName = t.getStringByField(Constants.CHANNEL_NAME);
						
		Boolean ad = HBaseUtils.getInstance().isChannelAdvertising(day_partsTable, ts * 1000, channel);
        
        if(ad != null && ad)
        	_collector.emit(new Values(minute, channelName, t.getIntegerByField(Constants.VIEWERS)));
        
        _collector.ack(t);
	}
}
