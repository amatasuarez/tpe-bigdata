package ar.edu.itba.bigdata.bolt;

import java.util.Map;

import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;


import ar.edu.itba.bigdata.utils.Constants;
import ar.edu.itba.bigdata.utils.HBaseUtils;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class TurnedOnTVsBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;

	public enum CustomerInfo {
		BOX_ID, FAMILY_GROUP, CLIENT_TYPE, NONE;
	}
	
	OutputCollector _collector;
	private HTable customerTable;
	
	private CustomerInfo info;
	
	public TurnedOnTVsBolt(CustomerInfo info){
		super();
		this.info = info;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        customerTable = HBaseUtils.getInstance().getHTable("customer");
    }
	
	@Override
	public void cleanup() {
		HBaseUtils.getInstance().closeHTable(customerTable);
	}

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	switch (info){
    	case BOX_ID:
    		declarer.declare(new Fields(Constants.MINUTE, Constants.BOX_ID, Constants.DIFF));
    		break;
    	case FAMILY_GROUP:
    		declarer.declare(new Fields(Constants.MINUTE, Constants.FAMILY_GROUP, Constants.DIFF));
    		break;
    	case CLIENT_TYPE:
			declarer.declare(new Fields(Constants.MINUTE, Constants.CLIENT_TYPE, Constants.DIFF));
			break;
    	default: // NONE
    		declarer.declare(new Fields(Constants.MINUTE, Constants.DIFF));
    		break;
    	}
    }

	@Override
	public void execute(Tuple t) {		
		Boolean power = t.getStringByField(Constants.POWER) != null ? "ON".equals(t.getStringByField(Constants.POWER)) : null;
						
		if (power != null){
			// Timestamp comes in seconds
			long minute = t.getLongByField(Constants.TIMESTAMP) / 60L;
			int box_id = t.getIntegerByField(Constants.BOX_ID);
			
			if (CustomerInfo.NONE.equals(info))
				_collector.emit(new Values(minute, power ? 1 : -1));
			else
				_collector.emit(new Values(minute, getInfoValue(box_id), power ? 1 : -1));
		}
		
        _collector.ack(t);
		
	}
	
	private String getInfoValue(int box_id){
		switch (info){
	    	case BOX_ID:
	    		return String.valueOf(box_id);
	    	case FAMILY_GROUP:
	    		return Bytes.toString(HBaseUtils.getInstance().getValue(customerTable, String.valueOf(box_id), "info", "family_group"));
	    	case CLIENT_TYPE:
	    		return Bytes.toString(HBaseUtils.getInstance().getValue(customerTable, String.valueOf(box_id), "info", "client_type"));
	    	default:
	    		return null;
    	}
	}

}
