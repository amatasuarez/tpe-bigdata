package ar.edu.itba.bigdata.bolt;

import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;


import ar.edu.itba.bigdata.utils.Constants;
import ar.edu.itba.bigdata.utils.HBaseUtils;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CountByChannelBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;
	
	// channel_id, accumulated_views
	private Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
	private long currentMinute = -1;

	private boolean emitRawValues;
	private HTable channelTable;
	
	public CountByChannelBolt(boolean emitRawValues){
		this.emitRawValues = emitRawValues;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        channelTable = HBaseUtils.getInstance().getHTable("channel");
    }
	
	@Override
	public void cleanup() {
		HBaseUtils.getInstance().closeHTable(channelTable);
	}

    @Override
    public void execute(Tuple t) {
    	long ts = t.getLongByField(Constants.TIMESTAMP);
    	long minute = ts / 60L;
    	
    	// If new minute, emit previous minute values
		if (currentMinute < minute) {
			if(currentMinute != -1) {
				for (Integer channel : counts.keySet()) {
					String channelName = Bytes.toString(HBaseUtils.getInstance().getValue(channelTable, String.valueOf(channel), "info", "name"));					
					if (emitRawValues)
						_collector.emit(new Values(ts, currentMinute, channel, channelName, counts.get(channel)));
					else
						_collector.emit(new Values(currentMinute, channelName, counts.get(channel)));
				}
			}
			currentMinute = minute;
		}
		
		int channel = t.getIntegerByField(Constants.CHANNEL);
    	Integer	accumulated = counts.get(channel);
		if(accumulated == null)
			accumulated = 0;
		
		accumulated += t.getIntegerByField(Constants.DIFF);
		counts.put(channel, accumulated);
		    	    	
        _collector.ack(t);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	if (emitRawValues)
    		declarer.declare(new Fields(Constants.TIMESTAMP, Constants.MINUTE, Constants.CHANNEL, Constants.CHANNEL_NAME, Constants.VIEWERS));
    	else
    		declarer.declare(new Fields(Constants.MINUTE, Constants.CHANNEL_NAME, Constants.VIEWERS));
    }
}