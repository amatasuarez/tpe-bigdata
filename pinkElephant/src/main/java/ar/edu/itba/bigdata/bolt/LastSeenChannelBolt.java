package ar.edu.itba.bigdata.bolt;

import java.util.HashMap;
import java.util.Map;

import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class LastSeenChannelBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;
	
	// box_id, last_seen_channel
	private Map<Integer, Integer> lastSeen = new HashMap<Integer, Integer>();
	
	@SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    @Override
    public void execute(Tuple t) {    	
    	long ts = t.getLongByField(Constants.TIMESTAMP);
    	int box_id = t.getIntegerByField(Constants.BOX_ID);
    	    	    	
    	Integer lastSeenChannel = lastSeen.get(box_id);    	
    	if(lastSeenChannel != null)
    		_collector.emit(new Values(ts, lastSeenChannel, -1));
    	
    	Integer newChannel = t.getIntegerByField(Constants.CHANNEL);
    	if(newChannel != null)
    		_collector.emit(new Values(ts, newChannel, 1));
    	
    	lastSeen.put(box_id, newChannel);
    	
        _collector.ack(t);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(new Fields(Constants.TIMESTAMP, Constants.CHANNEL, Constants.DIFF));    	
    }
}