package ar.edu.itba.bigdata.bolt;

import java.util.Map;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class JsonBolt extends BaseRichBolt {
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;

	
    @SuppressWarnings("rawtypes")
	@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {    	
    	String jsonText = tuple.getStringByField(Constants.JSON);
    	
    	JSONObject json = (JSONObject) JSONSerializer.toJSON(jsonText);
    	long timestamp = json.getLong("timestamp");
		int box_id = json.getInt("box_id");
		String power = null;
		if(json.has("power"))
			power = json.getString("power");
		Integer channel = null; 
		if(json.has("channel"))
			channel = json.getInt("channel");
		
    	_collector.emit(new Values(timestamp, box_id, power, channel));
    	
        _collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(Constants.TIMESTAMP, Constants.BOX_ID, Constants.POWER, Constants.CHANNEL));
    }
}