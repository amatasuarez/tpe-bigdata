package ar.edu.itba.bigdata;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;


import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.testing.TestWordSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class MeditionsSpout extends BaseRichSpout {
	private static final long serialVersionUID = 1L;
	
	public static Logger LOG = Logger.getLogger(TestWordSpout.class);
    boolean _isDistributed;
    SpoutOutputCollector _collector;

    private Connection connection;
    private MessageConsumer consumer;
  
    public MeditionsSpout() {
        this(true);
    }

    public MeditionsSpout(boolean isDistributed) {
        _isDistributed = isDistributed;
    }
        
    @SuppressWarnings("rawtypes")
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        _collector = collector;
        
        // Getting JMS connection from the server
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Constants.JMS_QUEUE_URL);
        Session session = null;
        try {
			connection = connectionFactory.createConnection();		
			connection.start();
			
	        // Creating session for receiving messages
	        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        
	        // MessageConsumer is used for receiving (consuming) messages
	        consumer = session.createConsumer(session.createQueue(Constants.JMS_QUEUE_NAME));	        
        } catch (JMSException e) {
        	Logger.getLogger(MeditionsSpout.class).fatal(e.getMessage());
        	try {
	        	if (connection != null)
	        		connection.close();
        	} catch (JMSException ex){
            	Logger.getLogger(MeditionsSpout.class).fatal(ex.getMessage());
        	}
        	try {
	        	if (consumer != null)
	        		consumer.close();
        	} catch (JMSException ex){
            	Logger.getLogger(MeditionsSpout.class).fatal(ex.getMessage());
        	}
		}
    }
    
    public void close() {
    	try {
    		consumer.close();
		} catch (JMSException e) {			
			Logger.getLogger(MeditionsSpout.class).fatal(e.getMessage());
		}
    	try {
    		connection.close();
		} catch (JMSException e) {			
			Logger.getLogger(MeditionsSpout.class).fatal(e.getMessage());
		}
    }
        
    public void nextTuple() {
    	try {
    		// Here we receive the message.
	        // By default this call is blocking, which means it will wait
	        // for a message to arrive on the queue.
    		TextMessage textMessage = (TextMessage) consumer.receive();	
	        String json = textMessage.getText();
	        
	        _collector.emit(new Values(json));
    	} catch (Exception e) {    		
    		Logger.getLogger(MeditionsSpout.class).fatal(e.getMessage());
    	}
    }
    
    public void ack(Object msgId) {

    }

    public void fail(Object msgId) {
        
    }
    
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("json"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        if(!_isDistributed) {
        	int parallelism = 2;
            Map<String, Object> ret = new HashMap<String, Object>();
            ret.put(Config.TOPOLOGY_MAX_TASK_PARALLELISM, parallelism);
            return ret;
        } else {
            return null;
        }
    }    
}