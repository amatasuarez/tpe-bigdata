package ar.edu.itba.bigdata;

import ar.edu.itba.bigdata.bolt.CountByAdsBolt;
import ar.edu.itba.bigdata.bolt.CountByCatBolt;
import ar.edu.itba.bigdata.bolt.CountByChannelBolt;
import ar.edu.itba.bigdata.bolt.CountByCustomerInfoBolt;
import ar.edu.itba.bigdata.bolt.CountTotalViewersBolt;
import ar.edu.itba.bigdata.bolt.JsonBolt;
import ar.edu.itba.bigdata.bolt.LastSeenCatBolt;
import ar.edu.itba.bigdata.bolt.LastSeenChannelBolt;
import ar.edu.itba.bigdata.bolt.FieldsDBOutputBolt;
import ar.edu.itba.bigdata.bolt.TurnedOnTVsBolt;
import ar.edu.itba.bigdata.bolt.TurnedOnTVsBolt.CustomerInfo;
import ar.edu.itba.bigdata.utils.Constants;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;

public class PinkElephantTopology {
	
	public static void main(String[] args) throws Exception {
    	
    	if (args.length > 1){
    		System.out.println("Cantidad de argumentos invalida.");
    		printUsage();
    		System.exit(-1);
    	}
    	
        TopologyBuilder builder = new TopologyBuilder();

        //build topology
        builder.setSpout(Constants.MEDITIONS_SPOUT, new MeditionsSpout());
        builder.setBolt(Constants.JSON_BOLT, new JsonBolt()).shuffleGrouping(Constants.MEDITIONS_SPOUT);
        
        String tableName = null;

    	// Televidentes por canal
    	tableName="ViewersByChannel";
        builder.setBolt(Constants.LAST_SEEN_CHANNEL_BOLT, new LastSeenChannelBolt(), 4)
        	.fieldsGrouping(Constants.JSON_BOLT, new Fields(Constants.BOX_ID));
        builder.setBolt(Constants.COUNT_BY_CHANNEL_BOLT, new CountByChannelBolt(false), 4)
        	.fieldsGrouping(Constants.LAST_SEEN_CHANNEL_BOLT, new Fields(Constants.CHANNEL));
        builder.setBolt("outBolt1", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.CHANNEL_NAME, Constants.VIEWERS))
        	.shuffleGrouping(Constants.COUNT_BY_CHANNEL_BOLT);
        
        // Televidentes por categoria
    	tableName="ViewersByCategory";
		builder.setBolt(Constants.LAST_SEEN_CAT_BOLT, new LastSeenCatBolt(), 2)
			.fieldsGrouping(Constants.JSON_BOLT, new Fields(Constants.BOX_ID));
		builder.setBolt(Constants.COUNT_BY_CAT_BOLT, new CountByCatBolt(), 2)
			.fieldsGrouping(Constants.LAST_SEEN_CAT_BOLT, new Fields(Constants.CATEGORY));
		builder.setBolt("outBolt2", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.CATEGORY, Constants.VIEWERS))
			.shuffleGrouping(Constants.COUNT_BY_CAT_BOLT);
		
		// Televidentes por grupo familiar
    	tableName="ViewersByFamilyGroup";
        builder.setBolt(Constants.TURNED_ON_TV_FAMILY_BOLT, new TurnedOnTVsBolt(CustomerInfo.FAMILY_GROUP), 4)
        	.shuffleGrouping(Constants.JSON_BOLT);
        builder.setBolt(Constants.COUNT_BY_FAMILY_BOLT, new CountByCustomerInfoBolt(CustomerInfo.FAMILY_GROUP), 2)
        	.fieldsGrouping(Constants.TURNED_ON_TV_FAMILY_BOLT, new Fields(Constants.FAMILY_GROUP));
        builder.setBolt("outBolt3", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.CUSTOMER_INFO, Constants.VIEWERS))
        	.shuffleGrouping(Constants.COUNT_BY_FAMILY_BOLT);

        // Televidentes por tipo de cliente
    	tableName="ViewersByClientType";
        builder.setBolt(Constants.TURNED_ON_TV_CLIENT_TYPE_BOLT, new TurnedOnTVsBolt(CustomerInfo.CLIENT_TYPE), 4)
        	.shuffleGrouping(Constants.JSON_BOLT);
        builder.setBolt(Constants.COUNT_BY_CLIENT_TYPE_BOLT, new CountByCustomerInfoBolt(CustomerInfo.CLIENT_TYPE), 2)
        	.fieldsGrouping(Constants.TURNED_ON_TV_CLIENT_TYPE_BOLT, new Fields(Constants.CLIENT_TYPE));
        builder.setBolt("outBolt4", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.CUSTOMER_INFO, Constants.VIEWERS))
        	.shuffleGrouping(Constants.COUNT_BY_CLIENT_TYPE_BOLT);
        
    	// Televidentes total
    	tableName="TotalViewers";
        builder.setBolt(Constants.TURNED_ON_TV_BOLT, new TurnedOnTVsBolt(CustomerInfo.NONE), 4)
        	.shuffleGrouping(Constants.JSON_BOLT);
        builder.setBolt(Constants.COUNT_BOLT, new CountTotalViewersBolt())
        	.shuffleGrouping(Constants.TURNED_ON_TV_BOLT);
        builder.setBolt("outBolt5", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.VIEWERS))
        	.shuffleGrouping(Constants.COUNT_BOLT);
        
    	// Televidentes viendo anuncios
    	tableName="ViewersWatchingAds";
        builder.setBolt(Constants.COUNT_BY_CHANNEL_RAW_BOLT, new CountByChannelBolt(true), 4)
        	.fieldsGrouping(Constants.LAST_SEEN_CHANNEL_BOLT, new Fields(Constants.CHANNEL));
        builder.setBolt(Constants.COUNT_BY_ADS_BOLT, new CountByAdsBolt(), 4)
        	.fieldsGrouping(Constants.COUNT_BY_CHANNEL_RAW_BOLT, new Fields(Constants.CHANNEL));
        builder.setBolt("outBolt6", new FieldsDBOutputBolt(tableName, Constants.MINUTE, Constants.CHANNEL_NAME, Constants.VIEWERS))
        	.shuffleGrouping(Constants.COUNT_BY_ADS_BOLT);
                
        Config conf = new Config();
        //conf.setDebug(true);
        
        if(args.length == 1) {
            conf.setNumWorkers(5);            
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        } else { 
	        LocalCluster cluster = new LocalCluster();
	        cluster.submitTopology("test", conf, builder.createTopology());
	        Utils.sleep(500000);
	        cluster.killTopology("test");
	        cluster.shutdown();    
        }
	        
    }
    
    private static void printUsage(){
		System.out.println("Usage: PinkElephantTopology <topology_name>");
    }
}