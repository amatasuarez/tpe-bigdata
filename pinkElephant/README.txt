Para generar la base de datos consulte el archivo scripts.sql

Compilar y generar el archivo jar usando Maven:
$> mvn assembly:assembly

Subir el archivo jar con las dependencias al hadoop-2013-datanode-4, que es donde se encuentra el Nimbus (el master node de Storm).

Ejecutar el siguiente comando eligiendo para <report_number> un valor entre 1 y 6:

[hadoop-2013-datanode-4]$> storm-0.8.2/bin/storm jar pinkElephant-0.0.1-SNAPSHOT-jar-with-dependencies.jar ar.edu.itba.bigdata.PinkElephantTopology <report_number> <topology_name>

+----------------------+
| Tables_in_test_aa    |
+----------------------+
| ViewersByChannel     |--> Reporte 1
| ViewersByCategory    |--> Reporte 2
| ViewersByFamilyGroup |--> Reporte 3
| ViewersByClientType  |--> Reporte 4
| TotalViewers         |--> Reporte 5
| ViewersWatchingAds   |--> Reporte 6
+----------------------+
