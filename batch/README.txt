# Inicialización de la BD en Hive.
$ init.sh

# Para ejecutar cualquiera de las consultas
$ hive --auxpath $HIVE_INSTALL/lib/hive-hbase-handler-0.9.0.jar,$HIVE_INSTALL/lib/hbase-0.92.0.jar,$HIVE_INSTALL/lib/zookeeper-3.4.3.jar -hiveconf hbase.zookeeper.quorum=hadoop-2013-datanode-1 -f path/to/query.sql

# Para generar los archivos de reporte
$ report-generator.sh


*** ACLARACIÓN ***:
 Si al ejecutar algunos de los archivos .sh incluídos en este trabajo se obtiene:
	/bin/sh^M : bad interpreter

 significa que es necesario convertirlos a formato Unix. Esto suele pasar por haber editado los
 archivos en otras plataformas, como por ejemplo, Windows, para luego correrlos en Unix.

 Para solucionar esto, se puede abrir el archivo en cuestión con el editor vim o vi, y en la consola del editor ejecutar:
	:set fileformat=unix
 
 Una vez guardado, ya se puede correr normalmente.

