-- Configure hive to print column’s headers
set hive.cli.print.header=true;

-- Load json as a string in a table
CREATE EXTERNAL TABLE json(d STRING) 
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\n'
STORED AS TEXTFILE LOCATION '/user/hadoop/TPE-aa/input';

-- Create table with one column per json’s property
CREATE EXTERNAL TABLE medition(box_id BIGINT, power STRING, channel INT, ts BIGINT);

-- Insert data from json table into medition table
INSERT OVERWRITE TABLE medition
SELECT get_json_object(json.d, '$.box_id') box_id, get_json_object(json.d, '$.power') power, get_json_object(json.d, '$.channel') channel, get_json_object(json.d, '$.timestamp') ts
FROM json;

-- Create the channel table using HBase information
CREATE EXTERNAL TABLE hbase_channel(key INT, name STRING)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = ":key,info:name")
TBLPROPERTIES ("hbase.table.name" = "channel");

-- Create the channel’s categories table using HBase information
CREATE EXTERNAL TABLE hbase_channel_cat_temp(key INT, categories STRING)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = ":key,info:categories")
TBLPROPERTIES ("hbase.table.name" = "channel");

CREATE EXTERNAL TABLE hbase_channel_cat(key INT, category STRING);
-- Insert a tuple for each value in the categories CSV
INSERT OVERWRITE TABLE hbase_channel_cat
SELECT key, category 
FROM hbase_channel_cat_temp 
LATERAL VIEW explode(split(categories, ',')) t1 AS category;

-- Create the day_parts table using HBase information
CREATE EXTERNAL TABLE hbase_day_parts(key INT, start_timestamp BIGINT, end_timestamp BIGINT, channel_number INT, title STRING, description STRING, categories STRING, type STRING)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = ":key,info:start_timestamp,info:end_timestamp,info:channel_number,info:title,info:description,info:categories,info:type")
TBLPROPERTIES ("hbase.table.name" = "day_parts");

-- Create the day_parts table using HBase information
CREATE EXTERNAL TABLE hbase_customer(key INT, family_group STRING, client_type STRING)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = ":key,info:family_group,info:client_type")
TBLPROPERTIES ("hbase.table.name" = "customer");