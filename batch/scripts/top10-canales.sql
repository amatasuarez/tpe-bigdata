-- Top 10 de canales en audiencia
SET hive.exec.compress.output=false;
DROP TABLE top10_canales;
CREATE TABLE top10_canales ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT hbase_channel.name, COUNT(box_id) total
FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key
GROUP BY hbase_channel.key, hbase_channel.name
ORDER BY total DESC
LIMIT 10;