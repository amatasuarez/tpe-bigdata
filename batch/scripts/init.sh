# DB initialization script.
#	Run from inside /input folder.

SCRIPTS=~/scripts
HIVE_AUXPATH=$HIVE_INSTALL/lib/hive-hbase-handler-0.9.0.jar,$HIVE_INSTALL/lib/hbase-0.92.0.jar,$HIVE_INSTALL/lib/zookeeper-3.4.3.jar
HIVE_CONF=hbase.zookeeper.quorum=hadoop-2013-datanode-1

hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/init.sql;