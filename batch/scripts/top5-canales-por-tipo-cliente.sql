-- Top 5 de canales en audiencia para cada tipo de cliente
SET hive.exec.compress.output=false;
DROP TABLE top5_canales_por_tipo_cliente;
CREATE TABLE top5_canales_por_tipo_cliente ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT * FROM (
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'Clasico'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'Digital'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'HD'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'Max HD'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'On Demand'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'Pay Per View'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
		UNION ALL
	SELECT client_type, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE client_type = 'Premium'
	GROUP BY client_type, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
) t;