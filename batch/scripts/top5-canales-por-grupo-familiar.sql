-- Top 5 de canales en audiencia para cada grupo familiar
SET hive.exec.compress.output=false;
DROP TABLE top5_canales_por_grupo_familiar;
CREATE TABLE top5_canales_por_grupo_familiar ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT * FROM (
	SELECT family_group, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE family_group = 'adulto'
	GROUP BY family_group, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT family_group, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE family_group = 'adultos en pareja'
	GROUP BY family_group, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT family_group, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE family_group = 'soltero'
	GROUP BY family_group, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT family_group, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE family_group = 'familia'
	GROUP BY family_group, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
	UNION ALL
	SELECT family_group, hbase_channel.name, COUNT(box_id) total
	FROM medition INNER JOIN hbase_channel ON medition.channel = hbase_channel.key INNER JOIN hbase_customer ON medition.box_id = hbase_customer.key
	WHERE family_group = 'jubilados'
	GROUP BY family_group, hbase_channel.name
	ORDER BY total DESC
	LIMIT 5
) t;