-- Cantidad de anuncios publicados por canal
SET hive.exec.compress.output=false;
DROP TABLE anuncios_por_canal;
CREATE TABLE anuncios_por_canal ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT c.name, COUNT(dp.key) 
FROM hbase_day_parts dp JOIN hbase_channel c ON dp.channel_number = c.key
WHERE dp.type LIKE 'ads'
GROUP BY dp.channel_number, c.name;