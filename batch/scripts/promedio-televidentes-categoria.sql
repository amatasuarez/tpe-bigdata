-- Promedio de tiempo dedicado por televidentes a cada categoría por día
SET hive.exec.compress.output=false;
DROP TABLE promedio_televidentes_categoria;
CREATE TABLE promedio_televidentes_categoria ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT t.day, t.category, AVG(t.delta) average
FROM (
SELECT from_unixtime(l.ts, 'yyyy-MM-dd') day, l.box_id box, cat.category category, MIN(r.ts - l.ts) delta
FROM medition l INNER JOIN medition r ON l.box_id = r.box_id INNER JOIN hbase_channel_cat cat ON cat.key = l.channel
WHERE r.ts - l.ts > 0
GROUP BY from_unixtime(l.ts, 'yyyy-MM-dd'), l.box_id, cat.category
) t
GROUP BY t.day, t.category;