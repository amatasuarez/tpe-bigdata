#!/bin/bash

#Report generator

SCRIPTS=~/scripts
REPORTS=~/reports
HIVE_AUXPATH=$HIVE_INSTALL/lib/hive-hbase-handler-0.9.0.jar,$HIVE_INSTALL/lib/hbase-0.92.0.jar,$HIVE_INSTALL/lib/zookeeper-3.4.3.jar
HIVE_CONF=hbase.zookeeper.quorum=hadoop-2013-datanode-1

rm -r $REPORTS/*;

hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/top10-canales.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/top10-categorias.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/anuncios-por-canal.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/promedio-televidentes-canal.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/promedio-televidentes-categoria.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/top5-canales-por-grupo-familiar.sql;
hive --auxpath $HIVE_AUXPATH -hiveconf $HIVE_CONF -f $SCRIPTS/top5-canales-por-tipo-cliente.sql;
 
hadoop fs -cat /user/hive/warehouse/top10_canales/* > $REPORTS/top_10_canales.csv;
hadoop fs -cat /user/hive/warehouse/top10_categorias/* > $REPORTS/top_10_categorias.csv;
hadoop fs -cat /user/hive/warehouse/anuncios_por_canal/* > $REPORTS/anuncios_por_canal.csv;
hadoop fs -cat /user/hive/warehouse/promedio_televidentes_canal/* > $REPORTS/promedio_televidentes_canal.csv;
hadoop fs -cat /user/hive/warehouse/promedio_televidentes_categoria/* > $REPORTS/promedio_televidentes_categoria.csv;
hadoop fs -cat /user/hive/warehouse/top5_canales_por_grupo_familiar/* > $REPORTS/top5_canales_por_grupo_familiar.csv;
hadoop fs -cat /user/hive/warehouse/top5_canales_por_tipo_cliente/* > $REPORTS/top5_canales_por_tipo_cliente.csv;