-- Top 10 de categorias en audiencia
SET hive.exec.compress.output=false;
DROP TABLE top10_categorias;
CREATE TABLE top10_categorias ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT hbase_channel_cat.category, COUNT(box_id) total
FROM medition INNER JOIN hbase_channel_cat ON medition.channel = hbase_channel_cat.key
GROUP BY hbase_channel_cat.category
ORDER BY total DESC
LIMIT 10;
