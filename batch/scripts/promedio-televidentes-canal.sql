-- Promedio de tiempo dedicado por televidentes a cada canal por día
SET hive.exec.compress.output=false;
DROP TABLE promedio_televidentes_canal;
CREATE TABLE promedio_televidentes_canal ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' AS
SELECT t.day, ch.name, AVG(t.delta) average 
FROM (
SELECT from_unixtime(l.ts, 'yyyy-MM-dd') day, l.box_id box, l.channel channel, MIN(r.ts - l.ts) delta
FROM medition l INNER JOIN medition r ON l.box_id = r.box_id
WHERE r.ts - l.ts > 0 
GROUP BY from_unixtime(l.ts, 'yyyy-MM-dd'), l.box_id, l.channel
) t INNER JOIN hbase_channel ch ON t.channel = ch.key
GROUP BY t.day, t.channel, ch.name;
